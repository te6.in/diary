import { Label } from "@/app/_components/Label";
import { j } from "@/app/_utils/utils";
import { Fragment } from "react";
import { UseFormRegisterReturn } from "react-hook-form";

export type TextFieldType =
  | "text"
  | "number"
  | "email"
  | "tel"
  | "datetime-local"
  | "url";

interface TextFieldProps {
  register: UseFormRegisterReturn;
  type: TextFieldType;
  id: string;
  label: string;
  placeholder?: string;
  prefix?: string;
  suffix?: string;
  isSubmitted?: boolean;
  required?: boolean;
  error?: string;
}

export function TextInput({
  register,
  type,
  id,
  label,
  placeholder,
  prefix,
  suffix,
  isSubmitted,
  required,
  error,
}: TextFieldProps) {
  const Tag = prefix ?? suffix ? "div" : Fragment;

  return (
    <div className="flex flex-col w-full">
      <div className="mb-1 flex flex-wrap justify-between px-1 text-xs font-medium">
        <Label text={label} id={id} required={required} />
        {error && (
          <span className="text-red-500 break-keep text-right motion-safe:animate-shake ml-auto">
            {error}
          </span>
        )}
      </div>
      <Tag
        {...((prefix ?? suffix) && { className: "flex rounded-md shadow-sm" })}
      >
        {prefix && (
          <span className="flex select-none items-center justify-center rounded-l-md border border-r-0 border-base-300 bg-base-100 px-3 pb-0.5 text-sm text-base-500">
            {prefix}
          </span>
        )}
        <input
          {...register}
          type={type}
          inputMode={
            type === "number"
              ? "numeric"
              : type === "url"
              ? "url"
              : type === "datetime-local"
              ? undefined
              : type
          }
          step={type === "datetime-local" ? 1 : undefined}
          id={id}
          className={j(
            "w-full appearance-none border px-3 py-2 text-base-900 placeholder-base-400 bg-base-50 focus-visible:outline-none focus-visible:ring-2 focus:border-transparent focus-visible:ring-accent-700",
            isSubmitted
              ? error
                ? "border-red-500 ring-2 ring-red-500"
                : "border-green-500 ring-2 ring-green-500"
              : "border-base-300",
            prefix && suffix
              ? ""
              : prefix
              ? "rounded-r-md"
              : suffix
              ? "rounded-l-md"
              : "rounded-md shadow-sm"
          )}
          placeholder={placeholder}
        />
        {suffix && (
          <span className="flex select-none items-center justify-center rounded-r-md border border-l-0 border-base-300 bg-base-100 px-3 pb-0.5 text-sm text-base-500">
            {suffix}
          </span>
        )}
      </Tag>
    </div>
  );
}
