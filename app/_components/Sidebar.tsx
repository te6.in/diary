"use client";

import { Button } from "@/app/_components/Button";
import { entriesState, sortState } from "@/app/_utils/atoms";
import { PencilLine, Search } from "lucide-react";
import { useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { v4 } from "uuid";

export function Sidebar() {
  const [entries, setEntries] = useRecoilState(entriesState);
  const router = useRouter();
  const queries = useSearchParams();
  const query = queries.get("q");
  const [queryState, setQueryState] = useState(query ?? "");
  const sortOption = useRecoilValue(sortState);

  const onAddClick = () => {
    const timestamp = new Date().toISOString();
    const id = v4();

    setEntries((entries) => [
      {
        id,
        title: "",
        body: "",
        timestamp,
        createdAt: timestamp,
        weather: "sunny",
        isDraft: true,
      },
      ...entries,
    ]);

    const list = document.querySelector("#list");

    if (!list) return;

    if (sortOption.includes("desc")) {
      list.scrollTo(0, 0);
    } else {
      setTimeout(() => {
        list.scrollTo(0, list.scrollHeight);
      }, 100);
    }

    router.push(`/entry/${id}/edit?new=true`);
  };

  const onSearchQueryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const query = e.target.value;

    setQueryState(query);

    if (query === "") {
      router.push("/");
      return;
    }

    router.push(`/?q=${query}`);
  };

  useEffect(() => {
    if (!query || query === "") {
      setQueryState("");
      return;
    }
  }, [router, query]);

  if (entries.length === 0) return null;

  return (
    <nav className="md:block hidden w-48 grid-in-nav pb-14 md:pb-4">
      <div className="h-full flex flex-col gap-2.5 pl-4 pt-12">
        <Button
          isPrimary
          isLoading={false}
          Icon={PencilLine}
          text="Add Entry"
          onClick={onAddClick}
        />
        <div className="relative">
          <span className="text-base-500 absolute top-1/2 -translate-y-1/2 left-2.5">
            <Search size={20} />
          </span>
          <input
            onChange={onSearchQueryChange}
            value={queryState}
            type="search"
            inputMode="search"
            className="w-full appearance-none border pl-9 pr-3 py-2 text-base-900 placeholder-base-400 bg-base-50 border-base-300 rounded-lg focus-visible:outline-none focus-visible:ring-2 focus:border-transparent focus-visible:ring-accent-700"
            placeholder="Search"
          />
        </div>
      </div>
    </nav>
  );
}
