import { Button } from "@/app/_components/Button";
import { AlertCircle, CheckCircle } from "lucide-react";

interface NotFoundProps {
  entryId: string;
}

export function NotFound({ entryId }: NotFoundProps) {
  return (
    <div className="flex flex-col gap-2 items-center justify-center h-full text-center text-base-700 px-10 text-balance">
      <AlertCircle size={48} />
      <div className="text-2xl font-bold">Entry not found</div>
      <div className="text-base-500 mt-2">
        Entry with id <strong className="font-bold">{entryId}</strong> does not
        exist.
      </div>
      <div className="text-base-500">
        It might have been deleted or you might have entered a wrong id.
      </div>
      <Button
        isLoading={false}
        text="Close error"
        className="max-w-xs mt-4"
        href="/"
        Icon={CheckCircle}
      />
    </div>
  );
}
