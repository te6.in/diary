"use client";

import { EntryList } from "@/app/_components/EntryList";
import { j } from "@/app/_utils/utils";
import { usePathname } from "next/navigation";

interface MainProps {
  children: React.ReactNode;
}

export function Main({ children }: MainProps) {
  const pathname = usePathname();
  const isEntryPage = pathname.startsWith("/entry/");

  return (
    <main
      className={j(
        "grid-in-main overflow-hidden md:pl-0 grid",
        isEntryPage ? "grid-cols-2" : ""
      )}
    >
      <EntryList isEntryPage={isEntryPage} />
      {children}
    </main>
  );
}
