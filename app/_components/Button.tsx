import { j } from "@/app/_utils/utils";
import { Loader2, LucideIcon } from "lucide-react";
import Link from "next/link";

interface ButtonProps {
  isPrimary?: boolean;
  isLoading: boolean;
  disabled?: boolean;
  Icon?: LucideIcon;
  iconFill?: boolean;
  text?: string;
  onClick?: any;
  href?: string;
  newTab?: boolean;
  className?: string;
}

export function Button({
  isPrimary,
  isLoading,
  disabled,
  Icon,
  iconFill,
  text,
  onClick,
  href,
  newTab,
  className,
}: ButtonProps) {
  const Tag = href ? Link : "button";

  return (
    <Tag
      href={disabled ? "" : href ?? ""}
      rel={newTab ? "noopener noreferrer" : undefined}
      target={newTab ? "_blank" : undefined}
      className={j(
        "flex items-center border justify-center rounded-lg px-4 py-2 w-full shadow-md transition-all focus-visible:outline-none focus-visible:ring-2 focus:border-transparent focus-visible:ring-accent-700",
        isPrimary
          ? "border-primary-500 bg-primary-500 font-medium text-white dark:text-stone-950"
          : "border-white dark:border-stone-950 bg-white dark:bg-stone-950 text-base-800",
        isLoading || disabled
          ? "cursor-not-allowed opacity-60"
          : isPrimary
          ? "hover:bg-primary-600 focus-visible:bg-primary-600 hover:border-primary-600 focus-visible:border-primary-600"
          : "hover:bg-base-100 focus-visible:bg-base-100 hover:border-base-100 focus-visible:border-base-100",
        disabled
          ? "cursor-not-allowed"
          : isLoading
          ? "cursor-progress"
          : "cursor-pointer active:scale-95",
        className ?? ""
      )}
      onClick={onClick}
      disabled={isLoading || disabled}
    >
      {isLoading ? (
        <Loader2 className="animate-spin flex-none" />
      ) : (
        Icon && (
          <Icon
            className="flex-none"
            strokeWidth={iconFill ? 1 : undefined}
            fill={iconFill ? "currentColor" : "transparent"}
          />
        )
      )}
      {text && (
        <span className="ml-2 mr-1 break-keep text-center leading-5">
          {text}
        </span>
      )}
    </Tag>
  );
}
