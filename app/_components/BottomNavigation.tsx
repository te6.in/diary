"use client";

import { entriesState, sortState } from "@/app/_utils/atoms";
import { Home, PencilLine } from "lucide-react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { v4 } from "uuid";

export function BottomNavigation() {
  const router = useRouter();
  const setEntries = useSetRecoilState(entriesState);
  const sortOption = useRecoilValue(sortState);

  const onAddClick = () => {
    const timestamp = new Date().toISOString();
    const id = v4();

    setEntries((entries) => [
      {
        id,
        title: "",
        body: "",
        timestamp,
        createdAt: timestamp,
        weather: "sunny",
        isDraft: true,
      },
      ...entries,
    ]);

    const list = document.querySelector("#list");

    if (!list) return;

    if (sortOption.includes("desc")) {
      list.scrollTo(0, 0);
    } else {
      setTimeout(() => {
        list.scrollTo(0, list.scrollHeight);
      }, 100);
    }

    router.push(`/entry/${id}/edit?new=true`);
  };

  return (
    <nav className="md:hidden grid grid-cols-2 h-16">
      <Link href="/" className="flex items-center justify-center">
        <Home size={20} />
      </Link>
      <button onClick={onAddClick} className="flex items-center justify-center">
        <PencilLine size={20} />
      </button>
    </nav>
  );
}
