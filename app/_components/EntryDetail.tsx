"use client";

import { Button } from "@/app/_components/Button";
import { DraftIndicator } from "@/app/_components/DraftIndicator";
import { EntryCard } from "@/app/_components/EntryCard";
import { FullPageOverlay } from "@/app/_components/FullPageOverlay";
import { ListHeader } from "@/app/_components/ListHeader";
import { NotFound } from "@/app/_components/NotFound";
import { PigeonMap } from "@/app/_components/PigeonMap";
import { Tag } from "@/app/_components/Tag";
import { entriesState } from "@/app/_utils/atoms";
import { getWeatherIcon, getYouTubeVideoId } from "@/app/_utils/entry";
import { getReadableTimestamp } from "@/app/_utils/localization";
import { AnimatePresence } from "framer-motion";
import { AlertTriangle, Search, Trash2 } from "lucide-react";
import { useRouter } from "next/navigation";
import { Fragment, useState } from "react";
import { useRecoilState } from "recoil";

interface EntryDetailProps {
  entryId: string;
}

// FIXME: 제목 hover 시 title로 풀 제목 표시

export function EntryDetail({ entryId }: EntryDetailProps) {
  const [entries, setEntries] = useRecoilState(entriesState);
  const entry = entries.find((entry) => entry.id === entryId);

  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showImageModal, setShowImageModal] = useState("");

  const router = useRouter();

  const WeatherIcon = entry ? getWeatherIcon(entry.weather) : Fragment;

  return (
    <>
      <AnimatePresence>
        {showDeleteModal && entry && (
          <FullPageOverlay
            type="close"
            buttonLabel="Nevermind"
            onCloseClick={() => setShowDeleteModal(false)}
            component={
              <div className="flex flex-col gap-4 items-center">
                <div className="flex flex-col gap-2 text-base-800 items-center text-center text-balance">
                  <AlertTriangle size={36} />
                  <div className="text-base-600 font-semibold">
                    Are you sure you want to delete this entry?
                  </div>
                </div>
                <div className="w-full">
                  <EntryCard entry={entry} isEntryPage={true} disabled />
                </div>
                <Button
                  isLoading={false}
                  Icon={Trash2}
                  text="Delete"
                  onClick={() => {
                    router.push("/");
                    setEntries(entries.filter((e) => e.id !== entryId));
                    setShowDeleteModal(false);
                  }}
                />
              </div>
            }
          />
        )}
        {showImageModal !== "" && entry && (
          <FullPageOverlay
            type="close"
            buttonLabel="Close"
            onCloseClick={() => setShowImageModal("")}
            component={
              <div className="flex flex-col gap-4 items-center">
                <div className="text-xl font-semibold text-base-800">
                  Image Preview
                </div>
                <img src={showImageModal} className="w-2/3 rounded-lg" />
                <a
                  className="underline text-sm font-semibold text-base-500"
                  href={showImageModal}
                  target="_blank"
                >
                  {showImageModal}
                </a>
              </div>
            }
          />
        )}
      </AnimatePresence>
      <div className="flex flex-col gap-2.5 h-full overflow-hidden col-span-2 md:col-span-1 pb-2 px-4 md:pb-4 md:pl-2 pt-1">
        {entry ? (
          <>
            <ListHeader
              as="h2"
              text={entry.title}
              viewing="detail"
              setShowDeleteModal={setShowDeleteModal}
            />
            <div className="bg-base-50 shadow-md rounded-2xl flex h-full overflow-hidden relative">
              <div className="overflow-auto flex flex-col gap-5 text-base-950 w-full pb-6">
                <div className="flex justify-between items-start gap-2 flex-wrap pl-4 pr-4 pt-4">
                  <div className="flex items-center gap-2 text-base-600 font-semibold flex-wrap pl-2 pt-1">
                    <WeatherIcon size={20} className="flex-none" />
                    <span className="text-sm">
                      {getReadableTimestamp(entry.timestamp)}
                    </span>
                  </div>
                  {entry.isDraft && <DraftIndicator />}
                </div>
                <div className="flex flex-col px-6 gap-2">
                  {entry.body.split("\n").map((paragraph, index) => (
                    <p key={index}>{paragraph}</p>
                  ))}
                </div>
                {entry.tags && entry.tags.length > 0 && (
                  <div className="flex gap-2 flex-wrap px-6">
                    {/* FIXME: key */}
                    {entry.tags.map((tag, index) => (
                      <Tag key={index} text={tag} clickable />
                    ))}
                  </div>
                )}
                {entry.media && (
                  <div className="px-6 flex flex-col gap-3">
                    {entry.media.images && entry.media.images.length > 0 && (
                      <div className="grid grid-cols-4 gap-2">
                        {entry.media.images.map((url, index) => (
                          <div
                            key={index}
                            className="relative rounded-lg overflow-hidden active:scale-95 transition-transform"
                          >
                            <button
                              className="absolute inset-0 bg-base-900 text-base-100 hover:opacity-80 opacity-0 flex transition-opacity items-center justify-center"
                              onClick={() => {
                                setShowImageModal(url);
                              }}
                            >
                              <Search size={28} />
                            </button>
                            <img
                              src={url}
                              className="aspect-square object-cover"
                            />
                          </div>
                        ))}
                      </div>
                    )}
                    {entry.media.location && (
                      <PigeonMap
                        position={{
                          latitude: entry.media.location.lat,
                          longitude: entry.media.location.lng,
                        }}
                        exact
                        fixed
                        className="h-80 rounded-lg"
                      />
                    )}
                    {entry.media.youtube && entry.media.youtube.url && (
                      <iframe
                        className="aspect-video w-full rounded-lg"
                        src={`https://www.youtube-nocookie.com/embed/${getYouTubeVideoId(
                          entry.media.youtube.url
                        )}`}
                        title="YouTube video player"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowFullScreen
                      ></iframe>
                    )}
                    {entry.media.appleMusic && entry.media.appleMusic.url && (
                      <iframe
                        className="h-[28.125rem] w-full rounded-lg"
                        src={entry.media.appleMusic.url.replace(
                          "https://music.apple.com",
                          "https://embed.music.apple.com"
                        )}
                        allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write"
                        sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation"
                      ></iframe>
                    )}
                  </div>
                )}
              </div>
            </div>
          </>
        ) : (
          <NotFound entryId={entryId} />
        )}
      </div>
    </>
  );
}
