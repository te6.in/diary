interface LabelProps {
  text: string;
  id: string | undefined;
  required?: boolean;
}

export function Label({ text, id, required }: LabelProps) {
  return (
    <label
      htmlFor={id}
      className="text-base-700 mr-1 break-keep uppercase text-xs font-semibold"
    >
      <span>{text}</span>
      {required && <span className="text-red-500 ml-1">*</span>}
    </label>
  );
}
