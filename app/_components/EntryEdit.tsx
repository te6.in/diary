"use client";

import { Button } from "@/app/_components/Button";
import { DraftIndicator } from "@/app/_components/DraftIndicator";
import { FullPageOverlay } from "@/app/_components/FullPageOverlay";
import { Label } from "@/app/_components/Label";
import { ListHeader } from "@/app/_components/ListHeader";
import { NotFound } from "@/app/_components/NotFound";
import { TextArea } from "@/app/_components/TextArea";
import { TextInput } from "@/app/_components/TextInput";
import { WeatherSelector } from "@/app/_components/WeatherSelector";
import { Entry } from "@/app/_types/type";
import { entriesState } from "@/app/_utils/atoms";
import { getYouTubeVideoId } from "@/app/_utils/entry";
import { getReadableTimestamp } from "@/app/_utils/localization";
import { titleCase } from "@/app/_utils/utils";
import { AnimatePresence } from "framer-motion";
import {
  AlertTriangle,
  ImageMinus,
  ImagePlus,
  Plus,
  PlusCircle,
  ScanSearch,
  X,
} from "lucide-react";
import { useRouter, useSearchParams } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useRecoilState } from "recoil";

interface EntryEditProps {
  entryId: string;
}

type EntryEditForm = Omit<Entry, "id" | "createdAt">;

export function EntryEdit({ entryId }: EntryEditProps) {
  const [entries, setEntries] = useRecoilState(entriesState);
  const entry = entries.find((entry) => entry.id === entryId);
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<Entry>({
    defaultValues: {
      ...entry,
      timestamp: (() => {
        if (!entry) return "";

        const date = new Date(entry.timestamp);

        date.setMinutes(date.getMinutes() - date.getTimezoneOffset());

        return date.toISOString().slice(0, 19);
      })(),
    },
  });

  const {
    register: registerImage,
    handleSubmit: handleSubmitImage,
    setValue: setImageValue,
    formState: { errors: errorsImage },
  } = useForm<{
    url: string;
  }>();

  const {
    register: registerTag,
    handleSubmit: handleSubmitTag,
    setValue: setTagValue,
    formState: { errors: errorsTag },
  } = useForm<{
    text: string;
  }>();

  const [showExitModal, setShowExitModal] = useState(false);
  const [showPhotoModal, setShowPhotoModal] = useState(false);
  const [showTagModal, setShowTagModal] = useState(false);
  const [previewYouTubeUrl, setPreviewYouTubeUrl] = useState("");
  const [previewAppleMusicUrl, setPreviewAppleMusicUrl] = useState("");
  const [previewImageUrl, setPreviewImageUrl] = useState("");
  const [temporaryImages, setTemporaryImages] = useState<string[]>(
    entry?.media?.images ?? []
  );
  const [temporaryTags, setTemporaryTags] = useState<string[]>(
    entry?.tags ?? []
  );

  const router = useRouter();
  const queries = useSearchParams();

  const isNew = queries.get("new") === true.toString();

  const onPreviewYouTube = (event: any) => {
    event.preventDefault();
    const url = getYouTubeVideoId(watch("media.youtube.url"));

    if (url) {
      setPreviewYouTubeUrl(url);
    }
  };

  const onPreviewAppleMusic = (event: any) => {
    event.preventDefault();
    const url = watch("media.appleMusic.url");

    if (url) {
      setPreviewAppleMusicUrl(url);
    }
  };

  const onImagePreview = (formData: { url: string }) => {
    setPreviewImageUrl(formData.url);
  };

  const onValid = async (formData: EntryEditForm) => {
    if (!entry) return;

    const { id, createdAt, tags } = entry;

    const newEntry: Entry = {
      ...formData,
      id,
      createdAt,
      tags:
        temporaryTags.length === 0
          ? formData.body
              .split(" ")
              .map((word) => word.replace(/[^a-zA-Z0-9ㄱ-ㅎㅏ-ㅣ가-힣]/g, ""))
              .filter((word) => word.length > 2)
              .map((word) => word.toLowerCase())
              .sort(() => Math.random() - 0.5)
              .slice(0, 8)
          : temporaryTags,
      isDraft: false,
      body: formData.body.trim().replace(/\n{2,}/g, "\n"),
      timestamp: new Date(formData.timestamp).toISOString(),
      media: {
        ...formData.media,
        images: temporaryImages,
      },
    };

    const youtubeTitle = await (async () => {
      if (newEntry?.media?.youtube?.url) {
        return fetch(
          `https://noembed.com/embed?dataType=json&url=${newEntry.media.youtube.url}`
        )
          .then((res) => res.json())
          .then((data) => {
            return data.title as string;
          });
      }
    })();

    const appleMusicTitle = (() => {
      if (newEntry?.media?.appleMusic?.url) {
        const url = decodeURI(newEntry.media.appleMusic.url);
        // https://music.apple.com/us/album/flu-game/1697584939?i=1697585810&l=ko

        // get flu-game from upper url

        const pathname = url.split("music.apple.com")[1];

        if (pathname.includes("/album/")) {
          return titleCase(
            pathname.split("/album/")[1].split("/")[0].replace(/-/g, " ")
          );
        }

        if (pathname.includes("/playlist/")) {
          return titleCase(
            pathname.split("/playlist/")[1].split("/")[0].replace(/-/g, " ")
          );
        }
      }
    })();

    if (youtubeTitle && newEntry?.media?.youtube?.url) {
      newEntry.media.youtube.title = youtubeTitle;
    }

    if (appleMusicTitle && newEntry?.media?.appleMusic?.url) {
      newEntry.media.appleMusic.title = appleMusicTitle;
    }

    const newEntries = [...entries].map((entry) =>
      entry.id === entryId ? newEntry : entry
    );

    setEntries(newEntries);

    router.push(`/entry/${entryId}`);
  };

  const onImageValid = (formData: { url: string }) => {
    if (!entry) return;

    setTemporaryImages([...temporaryImages, formData.url]);
    setShowPhotoModal(false);
    setImageValue("url", "");
    setPreviewImageUrl("");
  };

  const onTagValid = (formData: { text: string }) => {
    if (!entry) return;

    setTemporaryTags([...temporaryTags, formData.text]);
    setTagValue("text", "");
    setShowTagModal(false);
  };

  return (
    <>
      <AnimatePresence>
        {showExitModal && entry && (
          <FullPageOverlay
            type="close"
            buttonLabel="Nevermind"
            onCloseClick={() => setShowExitModal(false)}
            component={
              <div className="flex flex-col gap-4 items-center">
                <div className="flex flex-col gap-2 text-base-800 items-center text-center text-balance">
                  <AlertTriangle size={36} />
                  <div className="text-base-600 font-semibold">
                    Are you sure you want to close this entry without saving
                    changes?
                  </div>
                </div>
                <Button
                  isLoading={false}
                  Icon={X}
                  text="Exit without saving changes"
                  onClick={() => {
                    router.push(`/entry/${entryId}`);
                    setShowExitModal(false);
                  }}
                />
              </div>
            }
          />
        )}
        {showPhotoModal && entry && (
          <FullPageOverlay
            type="close"
            buttonLabel="Nevermind"
            onCloseClick={() => {
              setShowPhotoModal(false);
              setPreviewImageUrl("");
            }}
            component={
              <div className="flex flex-col gap-4 items-center">
                <TextInput
                  id="photo-url"
                  label="Image URL"
                  register={registerImage("url", {
                    required: "Image URL is required",
                  })}
                  type="url"
                  error={errorsImage.url?.message}
                  placeholder="https://i.imgur.com/NhcCdzQ.jpeg"
                  required
                />
                <Button
                  Icon={ScanSearch}
                  text="Preview"
                  isLoading={false}
                  onClick={handleSubmitImage(onImagePreview)}
                />
                {previewImageUrl && (
                  <img src={previewImageUrl} className="rounded-lg w-2/3" />
                )}
                <Button
                  isPrimary
                  isLoading={false}
                  Icon={ImagePlus}
                  text="Add this photo"
                  onClick={handleSubmitImage(onImageValid)}
                />
              </div>
            }
          />
        )}
        {showTagModal && entry && (
          <FullPageOverlay
            type="close"
            buttonLabel="Nevermind"
            onCloseClick={() => {
              setShowTagModal(false);
            }}
            component={
              <div className="flex flex-col gap-4 items-center">
                <TextInput
                  id="tag-input"
                  label="New Tag"
                  register={registerTag("text", {
                    required: "Tag is required",
                  })}
                  type="text"
                  error={errorsTag.text?.message}
                  placeholder="travel"
                  required
                />
                <Button
                  isPrimary
                  isLoading={false}
                  Icon={PlusCircle}
                  text="Add this tag"
                  onClick={handleSubmitTag(onTagValid)}
                />
              </div>
            }
          />
        )}
      </AnimatePresence>
      <div className="flex h-full w-full overflow-hidden col-span-2 md:col-span-1 pb-2 px-4 md:pb-4 md:pl-2 pt-1">
        {entry ? (
          <form
            className="flex flex-col gap-2.5 h-full w-full"
            onSubmit={(event) => {
              handleSubmit(onValid)(event);
            }}
          >
            <ListHeader
              as="div"
              text={isNew ? "Add Entry" : "Edit Entry"}
              viewing="edit"
              setShowExitModal={setShowExitModal}
            />
            <div className="bg-base-50 shadow-md rounded-2xl flex h-full overflow-hidden relative">
              <div className="overflow-auto flex flex-col gap-2 text-base-950 w-full">
                <div className="flex justify-between items-start gap-2 flex-wrap pl-4 pr-4 pt-4">
                  <div className="flex gap-2 text-base-400 font-semibold flex-wrap pl-2 pt-1 text-sm">
                    Created at {getReadableTimestamp(entry.createdAt)}
                  </div>
                  {entry.isDraft && <DraftIndicator />}
                </div>
                <div className="px-5 pb-5 flex flex-col gap-3">
                  <div className="font-bold">
                    <TextInput
                      register={register("title", {
                        required: "Title is required",
                      })}
                      id="title"
                      label="Title"
                      placeholder="Cool Title"
                      type="text"
                      error={errors.title?.message}
                      required
                    />
                  </div>
                  <TextInput
                    register={register("timestamp", {
                      required: "Timestamp is required",
                    })}
                    type="datetime-local"
                    id="timestamp"
                    label="Timestamp"
                    error={errors.timestamp?.message}
                    required
                  />
                  <WeatherSelector
                    register={register("weather", {
                      required: "Weather is required",
                    })}
                    error={errors.weather?.message}
                  />
                  <TextArea
                    register={register("body", {
                      required: "Body is required",
                    })}
                    id="body"
                    label="Body"
                    rows={10}
                    placeholder="Today I..."
                    error={errors.body?.message}
                    required
                  />
                  <div className="flex flex-col gap-1">
                    <Label text="Tags" id="tags-label" />
                    <div className="flex gap-2 flex-wrap">
                      {/* FIXME: key */}
                      {temporaryTags.map((tag, index) => (
                        <button
                          type="button"
                          key={index}
                          onClick={() => {
                            setTemporaryTags(
                              temporaryTags.filter((_, i) => i !== index)
                            );
                          }}
                          className="rounded-lg px-2 py-1 font-semibold text-sm bg-base-200 flex gap-0.5 items-center text-base-600 hover:bg-base-300 transition-all active:scale-95"
                        >
                          <span>{tag}</span>
                          <X size={16} />
                        </button>
                      ))}
                      <button
                        type="button"
                        onClick={() => {
                          setShowTagModal(true);
                        }}
                        className="rounded-lg px-2 py-1 font-semibold text-sm bg-base-700 flex gap-0.5 items-center text-base-200 hover:bg-base-800 transition-all active:scale-95"
                      >
                        <span>Add tag</span>
                        <Plus size={16} />
                      </button>
                    </div>
                    <div className="text-base-400 text-xs font-medium mt-1 ml-0.5">
                      If no tag is provided, the AI will generate tags based on
                      the body, for you.
                    </div>
                  </div>
                  <div className="flex flex-col gap-1">
                    <Label text="Photos" id="photos-label" />{" "}
                    <div className="grid grid-cols-4 gap-2">
                      {temporaryImages.map((url, index) => (
                        <div
                          key={index}
                          className="relative rounded-lg overflow-hidden transition-transform active:scale-95"
                        >
                          <button
                            type="button"
                            className="absolute inset-0 bg-base-900 text-base-100 hover:opacity-80 opacity-0 flex flex-col gap-1 items-center justify-center transition-opacity text-sm font-medium"
                            onClick={() => {
                              setTemporaryImages(
                                temporaryImages.filter((_, i) => i !== index)
                              );
                            }}
                          >
                            <ImageMinus size={28} />
                            <span>Remove photo</span>
                          </button>
                          <img
                            src={url}
                            className="aspect-square object-cover"
                          />
                        </div>
                      ))}
                      <button
                        type="button"
                        className="aspect-square bg-base-200 rounded-lg flex flex-col justify-center items-center gap-1 text-base-500 font-medium text-sm transition-all active:scale-95 hover:bg-base-300"
                        onClick={() => {
                          setShowPhotoModal(true);
                        }}
                      >
                        <ImagePlus size={28} />
                        <span>Add photo</span>
                      </button>
                    </div>
                  </div>
                  <TextInput
                    register={register("media.youtube.url", {
                      validate: (value) => {
                        if (!value) return true;
                        const id = getYouTubeVideoId(value);
                        if (!id) return "Invalid YouTube URL";
                        return true;
                      },
                    })}
                    id="youtube"
                    label="YouTube Video"
                    placeholder="https://www.youtube.com/watch?v=6Ezwfsx9JEA or many other formats"
                    type="url"
                    error={errors.media?.youtube?.url?.message}
                  />
                  <Button
                    isPrimary
                    text="Preview"
                    isLoading={false}
                    onClick={onPreviewYouTube}
                    disabled={!watch("media.youtube.url")}
                  />
                  {previewYouTubeUrl && (
                    <iframe
                      className="aspect-video w-full rounded-lg"
                      src={`https://www.youtube-nocookie.com/embed/${getYouTubeVideoId(
                        previewYouTubeUrl
                      )}`}
                      title="YouTube video player"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                      allowFullScreen
                    ></iframe>
                  )}
                  <TextInput
                    register={register("media.appleMusic.url", {
                      validate: (value) => {
                        if (!value) return true;

                        if (value.includes("music.apple.com") === false) {
                          return "Invalid Apple Music URL";
                        }

                        return true;
                      },
                    })}
                    id="apple-music"
                    label="Apple Music Song, Album, or Playlist"
                    placeholder="https://music.apple.com/us/album/permanent-damage/1638935082 or many other formats"
                    type="url"
                    error={errors.media?.appleMusic?.url?.message}
                  />
                  <Button
                    isPrimary
                    text="Preview"
                    isLoading={false}
                    onClick={onPreviewAppleMusic}
                    disabled={!watch("media.appleMusic.url")}
                  />
                  {previewAppleMusicUrl && (
                    <iframe
                      className="h-[28.125rem] w-full rounded-lg"
                      src={previewAppleMusicUrl.replace(
                        "https://music.apple.com",
                        "https://embed.music.apple.com"
                      )}
                      allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write"
                      sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation"
                    ></iframe>
                  )}
                </div>
              </div>
            </div>
          </form>
        ) : (
          <NotFound entryId={entryId} />
        )}
      </div>
    </>
  );
}
