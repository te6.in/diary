import { Label } from "@/app/_components/Label";
import { Entry, WEATHERS } from "@/app/_types/type";
import { getWeatherIcon } from "@/app/_utils/entry";
import { UseFormRegisterReturn } from "react-hook-form";

interface WeatherSelectorProps {
  register: UseFormRegisterReturn;
  error?: string;
}

export function WeatherSelector({ register, error }: WeatherSelectorProps) {
  return (
    <div className="flex flex-col">
      <div className="mb-1 flex justify-between px-1 text-xs font-medium">
        <Label text="Weather" id="weather" required />
        {error && (
          <span className="text-red-500 motion-safe:animate-shake">
            {error}
          </span>
        )}
      </div>
      <fieldset className="grid gap-1.5 grid-cols-4 p-2 pb-1.5 rounded-md shadow-sm border-base-300 bg-white dark:bg-stone-950 border">
        {WEATHERS.map((weather) => (
          <Input key={weather} register={register} weather={weather} />
        ))}
      </fieldset>
    </div>
  );
}

interface InputProps {
  weather: Entry["weather"];
  register: UseFormRegisterReturn;
}

function Input({ weather, register }: InputProps) {
  const Icon = getWeatherIcon(weather);

  return (
    <label className="flex flex-col gap-1 active:scale-95 transition-transform">
      <div className="relative">
        <input
          {...register}
          className="flex items-center justify-center rounded h-10 w-full checked:bg-primary-500 checked:hover:bg-primary-500 focus-within:checked:bg-primary-500 shadow-sm border-base-300 checked:bg-none peer transition-colors bg-white dark:bg-stone-950 focus-within:ring-2 focus-within:ring-accent-700"
          name="weather"
          type="radio"
          value={weather}
        />
        <Icon
          className="absolute top-1/2 -translate-y-1/2 left-1/2 -translate-x-1/2 text-base-700 peer-checked:text-white"
          width={20}
          height={20}
        />
      </div>
      <div className="break-keep px-0.5 text-xs text-base-500 font-medium text-center capitalize">
        {weather}
      </div>
    </label>
  );
}
