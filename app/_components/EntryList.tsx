import { Button } from "@/app/_components/Button";
import { EntryCard } from "@/app/_components/EntryCard";
import { ListHeader } from "@/app/_components/ListHeader";
import { entriesState, sortState } from "@/app/_utils/atoms";
import { j } from "@/app/_utils/utils";
import { chosungIncludes, hangulIncludes } from "@toss/hangul";
import { AnimatePresence, motion } from "framer-motion";
import { BookCheck, BookMarked, PencilLine, SearchX } from "lucide-react";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { v4 } from "uuid";

interface EntryListProps {
  isEntryPage: boolean;
}

export type SortOptions =
  | "timestamp-asc"
  | "timestamp-desc"
  | "createdAt-asc"
  | "createdAt-desc";

export function EntryList({ isEntryPage }: EntryListProps) {
  const pathname = usePathname();
  const router = useRouter();
  const queries = useSearchParams();
  const query = queries.get("q");

  const [entries, setEntries] = useRecoilState(entriesState);

  const [showingEntries, setShowingEntries] = useState(entries);
  const [sortOption, setSortOption] = useRecoilState(sortState);

  const currentEntryId = pathname.split("/")[2];

  useEffect(() => {
    const filteredEntries = entries.filter((entry) => {
      if (!query || query === "") return true;

      return (
        entry.title.toLowerCase().includes(query.toLowerCase()) ||
        entry.body.toLowerCase().includes(query.toLowerCase()) ||
        hangulIncludes(entry.title, query) ||
        hangulIncludes(entry.body, query) ||
        chosungIncludes(entry.title, query) ||
        chosungIncludes(entry.body, query)
      );
    });

    switch (sortOption) {
      case "timestamp-asc":
        setShowingEntries(
          [...filteredEntries].sort((a, b) => {
            return (
              new Date(a.timestamp).getTime() - new Date(b.timestamp).getTime()
            );
          })
        );
        break;
      case "timestamp-desc":
        setShowingEntries(
          [...filteredEntries].sort((a, b) => {
            return (
              new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime()
            );
          })
        );
        break;
      case "createdAt-asc":
        setShowingEntries(
          [...filteredEntries].sort((a, b) => {
            return (
              new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
            );
          })
        );
        break;
      case "createdAt-desc":
        setShowingEntries(
          [...filteredEntries].sort((a, b) => {
            return (
              new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
            );
          })
        );
        break;
    }
  }, [sortOption, entries, query]);

  const onAddClick = () => {
    const timestamp = new Date().toISOString();
    const id = v4();

    setEntries((entries) => [
      {
        id,
        title: "",
        body: "",
        timestamp,
        createdAt: timestamp,
        weather: "sunny",
        isDraft: true,
      },
      ...entries,
    ]);

    const list = document.querySelector("#list");

    if (!list) return;

    if (sortOption.includes("desc")) {
      list.scrollTo(0, 0);
    } else {
      setTimeout(() => {
        list.scrollTo(0, list.scrollHeight);
      }, 100);
    }

    router.push(`/entry/${id}/edit?new=true`);
  };

  return showingEntries.length > 0 ? (
    <div
      className={j(
        "flex-col gap-2.5 h-full overflow-hidden pb-2 md:pb-4 px-4 pt-1",
        isEntryPage ? "md:flex md:pr-2 hidden" : "flex"
      )}
    >
      <ListHeader
        as={isEntryPage ? "div" : "h2"}
        text={query ? `Search results for ${query}` : "All entries"}
        viewing="list"
      />
      <div className="bg-base-200 rounded-2xl flex h-full overflow-hidden">
        <ol
          className="p-3 overflow-auto flex flex-col gap-3 w-full scroll-smooth"
          id="list"
        >
          <AnimatePresence>
            {showingEntries.map((entry) => (
              <motion.li
                key={entry.id}
                initial={{ opacity: 0, x: -100 }}
                animate={{ opacity: 1, x: 0 }}
                exit={{ opacity: 0, x: 100 }}
              >
                <EntryCard
                  entry={entry}
                  isEntryPage={isEntryPage}
                  current={currentEntryId === entry.id}
                />
              </motion.li>
            ))}
          </AnimatePresence>
        </ol>
      </div>
    </div>
  ) : query !== null && query !== "" ? (
    <div
      className={j(
        "flex-col gap-2 items-center justify-center h-full text-center text-base-700 px-10 text-balance",
        isEntryPage ? "md:flex md:pr-2 hidden" : "flex"
      )}
    >
      <SearchX size={48} />
      <p className="text-2xl font-bold">No results found for {query}</p>
      <p className="text-base-500 mt-2">
        Try searching for something else or go back to see all entries.
      </p>
      <Button
        isPrimary
        isLoading={false}
        text="See all entries"
        className="max-w-xs mt-4"
        href="/"
        Icon={BookMarked}
      />
    </div>
  ) : (
    <div
      className={j(
        "flex-col gap-2 items-center justify-center h-full text-center text-base-700 px-10 text-balance",
        isEntryPage ? "md:flex md:pr-2 hidden" : "flex"
      )}
    >
      <BookMarked size={48} />
      <p className="text-2xl font-bold">Your diary is empty</p>
      <p className="text-base-500 mt-2">
        Click the button below to add your first entry.
      </p>
      <div className="flex flex-col gap-2 mt-4">
        <Button
          isPrimary
          isLoading={false}
          text="Load Example Entries"
          onClick={async () => {
            const entries = await fetch("/example.json").then((res) =>
              res.json()
            );
            setEntries(entries);
          }}
          Icon={BookCheck}
        />
        <Button
          isLoading={false}
          text="Add Entry"
          onClick={onAddClick}
          Icon={PencilLine}
        />
      </div>
    </div>
  );
}
