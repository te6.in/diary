"use client";

import { ThemeProvider } from "next-themes";
import { RecoilRoot } from "recoil";

interface ProvidersProps {
  children: React.ReactNode;
}

export function Providers({ children }: ProvidersProps) {
  return (
    <RecoilRoot>
      <ThemeProvider attribute="class">{children}</ThemeProvider>
    </RecoilRoot>
  );
}
