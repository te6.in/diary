import { j } from "@/app/_utils/utils";
import Link from "next/link";

interface TagProps {
  clickable?: boolean;
  text: string;
  isCountIndicator?: boolean;
}

export function Tag({ clickable, text, isCountIndicator }: TagProps) {
  const Tag = clickable ? Link : "div";

  return (
    <Tag
      href={clickable ? `/?q=${text}` : ""}
      className={j(
        "rounded-lg px-2 py-1 font-semibold text-sm bg-base-200",
        isCountIndicator ? "text-base-400" : "text-base-600",
        clickable
          ? "hover:bg-base-300 active:bg-base-400 active:scale-95 transition-all"
          : ""
      )}
    >
      {text}
    </Tag>
  );
}
