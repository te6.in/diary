"use client";

import { Button } from "@/app/_components/Button";
import { EntryCard } from "@/app/_components/EntryCard";
import { FullPageOverlay } from "@/app/_components/FullPageOverlay";
import { ThemeChanger } from "@/app/_components/ThemeChanger";
import { entriesState } from "@/app/_utils/atoms";
import { AnimatePresence } from "framer-motion";
import { AlertTriangle, FileDown, FileUp, Import } from "lucide-react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useRecoilState } from "recoil";

export function Header() {
  const [entries, setEntries] = useRecoilState(entriesState);
  const [showImportModal, setShowImportModal] = useState(false);
  const [fileString, setFileString] = useState<string | null>(null);
  const router = useRouter();

  const onDownloadClick = () => {
    const element = document.createElement("a");
    const file = new Blob([JSON.stringify(entries)], {
      type: "application/json",
    });
    element.href = URL.createObjectURL(file);
    element.download = `Diary ${new Date().toISOString()}.json`;
    document.body.appendChild(element);
    element.click();
  };

  const onImportFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];

    if (!file) return;

    const reader = new FileReader();

    reader.onload = () => {
      setShowImportModal(true);
      setFileString(reader.result as string);
    };

    reader.readAsText(file);
  };

  return (
    <>
      <AnimatePresence>
        {showImportModal && fileString && (
          <FullPageOverlay
            type="close"
            buttonLabel="Nevermind"
            onCloseClick={() => {
              setShowImportModal(false);
              setFileString(null);
            }}
            component={(() => {
              const parsed = JSON.parse(fileString);

              return (
                <div className="flex flex-col gap-4 items-center">
                  <div className="flex flex-col gap-2 text-base-800 items-center text-center text-balance">
                    <AlertTriangle size={36} />
                    <div className="text-base-600 font-semibold">
                      Are you sure you want to overwrite your diary with the
                      imported one?
                    </div>
                    <div className="text-base-500 text-sm">
                      The imported diary contains {parsed.length} entries.
                    </div>
                    <div className="w-full flex flex-col gap-4 my-3">
                      <EntryCard
                        entry={parsed[0]}
                        isEntryPage={true}
                        disabled
                      />
                      {parsed.length > 1 && (
                        <div className="text-base-500 text-xs font-semibold">
                          and {parsed.length - 1} more entries.
                        </div>
                      )}
                    </div>
                  </div>
                  <Button
                    isPrimary
                    isLoading={false}
                    Icon={Import}
                    text="Overwrite and import"
                    onClick={() => {
                      setEntries(parsed);
                      router.push("/");
                      setShowImportModal(false);
                      setFileString(null);
                    }}
                  />
                </div>
              );
            })()}
          />
        )}
      </AnimatePresence>
      <header className="flex h-16 items-center p-2 grid-in-header text-base-950 justify-between">
        <h1>
          <Link
            href="/"
            className="flex rounded-xl text-xl hover:bg-base-200 px-4 py-3 transition-all font-semibold active:scale-95 focus-visible:bg-base-200 focus-visible:ring-2 focus:border-transparent focus-visible:ring-accent-700 focus-visible:outline-none"
          >
            Diary
          </Link>
        </h1>
        <div className="flex">
          <ThemeChanger />
          <button
            onClick={onDownloadClick}
            aria-label="Download diary as JSON file"
            className="flex-none aspect-square p-3 hover:bg-base-200 transition-all rounded-xl active:scale-95 focus-visible:bg-base-200 focus-visible:ring-2 focus:border-transparent focus-visible:ring-accent-700 focus-visible:outline-none"
          >
            <FileDown />
          </button>
          <label
            aria-label="Import diary from JSON file"
            className="flex-none aspect-square p-3 hover:bg-base-200 transition-all rounded-xl active:scale-95 focus-visible:bg-base-200 focus-visible:ring-2 focus:border-transparent focus-visible:ring-accent-700 focus-visible:outline-none"
          >
            <input
              type="file"
              accept=".json"
              className="hidden"
              onChange={onImportFileChange}
            />
            <FileUp />
          </label>
        </div>
      </header>
    </>
  );
}
