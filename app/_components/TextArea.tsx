import { Label } from "@/app/_components/Label";
import { UseFormRegisterReturn } from "react-hook-form";

interface TextAreaProps {
  register: UseFormRegisterReturn;
  id: string;
  label: string;
  rows: number;
  placeholder: string;
  required?: boolean;
  error?: string;
}

export function TextArea({
  register,
  id,
  label,
  rows,
  placeholder,
  required,
  error,
}: TextAreaProps) {
  return (
    <div className="flex flex-col">
      <div className="mb-1 flex justify-between px-1 text-xs font-medium">
        <Label text={label} id={id} required={required} />
        {error && (
          <span className="text-red-500 motion-safe:animate-shake">
            {error}
          </span>
        )}
      </div>
      <textarea
        {...register}
        rows={rows}
        id={id}
        placeholder={placeholder}
        className="w-full resize-none appearance-none rounded-md border border-base-300 px-3 py-2 text-base-900 placeholder-base-400 shadow-sm bg-white dark:bg-stone-950 focus-visible:ring-2
        focus-visible:ring-accent-700 focus:border-transparent"
      />
    </div>
  );
}
