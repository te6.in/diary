import { DraftIndicator } from "@/app/_components/DraftIndicator";
import { Tag } from "@/app/_components/Tag";
import { Entry } from "@/app/_types/type";
import { getWeatherIcon } from "@/app/_utils/entry";
import { getReadableTimestamp } from "@/app/_utils/localization";
import { j } from "@/app/_utils/utils";
import { MapPin, Music, Play, Youtube } from "lucide-react";
import Link from "next/link";

interface EntryCardProps {
  entry: Entry;
  isEntryPage: boolean;
  disabled?: boolean;
  current?: boolean;
}

interface MediaChipProps {
  type: "location" | "youtube" | "appleMusic";
  text: string;
}

function MediaChip({ type, text }: MediaChipProps) {
  const Icon = (function () {
    switch (type) {
      case "location":
        return MapPin;
      case "youtube":
        return Youtube;
      case "appleMusic":
        return Music;
      default:
        return Play;
    }
  })();

  const colors = (function () {
    switch (type) {
      case "location":
        return "bg-blue-200 dark:bg-blue-950 text-blue-600 dark:text-blue-500";
      case "youtube":
        return "bg-red-200 dark:bg-red-950 text-red-600 dark:text-red-500";
      case "appleMusic":
        return "bg-rose-200 dark:bg-rose-950 text-rose-600 dark:text-rose-500";
      default:
        return "bg-green-200 dark:bg-green-950 text-green-600 dark:text-green-500";
    }
  })();

  return (
    <div
      className={j("flex items-center gap-1.5 rounded-lg px-2.5 py-2", colors)}
    >
      <Icon className="flex-none" size={20} />
      <span>{text}</span>
    </div>
  );
}

export function EntryCard({
  entry,
  isEntryPage,
  disabled,
  current,
}: EntryCardProps) {
  const TitleTag = isEntryPage ? "div" : "h3";
  const MainTag = disabled ? "div" : Link;

  const WeatherIcon = getWeatherIcon(entry.weather);

  return (
    <MainTag
      href={disabled !== true ? `/entry/${entry.id}` : ""}
      className={j(
        "py-5 rounded-xl shadow-lg h-auto border-2 min-h-0 flex text-start items-start gap-2 flex-col transition-all relative overflow-hidden",
        current === true ? "border-base-500" : "border-transparent",
        disabled !== true
          ? "bg-base-50 hover:bg-base-100 focus-visible:bg-base-100 focus-visible:ring-2 focus-visible:ring-accent-700 focus-visible:outline-none active:scale-[0.975]"
          : "bg-white dark:bg-stone-950"
      )}
    >
      <div className="px-5 flex flex-col gap-2">
        {entry.isDraft && <DraftIndicator absolute />}
        <div className="flex items-center gap-1 text-base-400 font-semibold flex-wrap">
          <WeatherIcon size={16} strokeWidth={2.5} className="flex-none" />
          <span className="text-xs">
            {getReadableTimestamp(entry.timestamp)}
          </span>
        </div>
        {entry.title.length > 0 ? (
          <TitleTag className="text-xl text-base-900 font-bold">
            {entry.title}
          </TitleTag>
        ) : (
          <TitleTag className="text-xl text-base-400 font-bold">
            Empty Title
          </TitleTag>
        )}
        {entry.body.length > 0 && (
          <p className="line-clamp-3 font-normal text-sm text-base-700">
            {entry.body}
          </p>
        )}
      </div>
      {entry.media && entry.media.images && entry.media.images.length > 0 && (
        <div className="px-5 flex gap-2 overflow-auto pb-2 mt-2">
          {/* FIXME: key */}
          {entry.media.images.map((url, index) => (
            <img
              src={url}
              key={index}
              className="aspect-square object-cover rounded-lg w-16"
            />
          ))}
        </div>
      )}
      {entry.media &&
        (entry.media.location ||
          (entry.media.youtube &&
            entry.media.youtube.url &&
            entry.media.youtube.title) ||
          (entry.media.appleMusic &&
            entry.media.appleMusic.url &&
            entry.media.appleMusic.title)) && (
          <div className="px-5 flex gap-2 flex-wrap">
            {entry.media.location && (
              <MediaChip type="location" text={entry.media.location.name} />
            )}
            {entry.media.youtube &&
              entry.media.youtube.url &&
              entry.media.youtube.title && (
                <MediaChip type="youtube" text={entry.media.youtube.title} />
              )}
            {entry.media.appleMusic &&
              entry.media.appleMusic.url &&
              entry.media.appleMusic.title && (
                <MediaChip
                  type="appleMusic"
                  text={entry.media.appleMusic.title}
                />
              )}
          </div>
        )}
      {entry.tags && entry.tags.length > 0 && (
        <div className="px-5 flex gap-2 flex-wrap mt-2">
          {/* FIXME: key */}
          {entry.tags.slice(0, 5).map((tag, index) => (
            <Tag key={index} text={tag} />
          ))}
          {entry.tags.length > 5 && (
            <Tag
              text={`+${entry.tags.length - 5}`}
              isCountIndicator={true}
            ></Tag>
          )}
        </div>
      )}
    </MainTag>
  );
}
