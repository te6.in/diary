import { sortState } from "@/app/_utils/atoms";
import {
  Check,
  LucideIcon,
  Pencil,
  SortAsc,
  SortDesc,
  Trash2,
  X,
} from "lucide-react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useRecoilState } from "recoil";

interface ListHeaderProps {
  viewing: "list" | "detail" | "edit";
  as: "h2" | "div";
  text: string;
  setShowDeleteModal?: React.Dispatch<React.SetStateAction<boolean>>;
  setShowExitModal?: React.Dispatch<React.SetStateAction<boolean>>;
}

interface ContextButtonProps {
  onClick?: () => void;
  type?: "button" | "submit";
  href?: string;
  Icon: LucideIcon;
  ariaLabel: string;
}

function ContextButton({
  onClick,
  type,
  href,
  Icon,
  ariaLabel,
}: ContextButtonProps) {
  const Tag = href ? Link : "button";

  return (
    <Tag
      type={type ?? "button"}
      href={href ?? ""}
      onClick={onClick}
      aria-label={ariaLabel}
      className="flex-none p-1.5 aspect-square hover:bg-base-200 transition-all rounded-lg active:scale-95 focus-visible:bg-base-200 focus-visible:ring-2 focus:border-none focus-visible:ring-accent-700 focus-visible:outline-none"
    >
      <Icon size={20} />
    </Tag>
  );
}

export function ListHeader({
  viewing,
  as,
  text,
  setShowDeleteModal,
  setShowExitModal,
}: ListHeaderProps) {
  const pathname = usePathname();
  const Tag = as;

  const entryId = pathname.replace("/entry/", "");
  const [sortOption, setSortOption] = useRecoilState(sortState);

  return (
    <div className="flex items-center text-base-600">
      {viewing === "detail" && (
        <ContextButton href="/" Icon={X} ariaLabel="Close entry detail" />
      )}
      {viewing === "edit" && setShowExitModal && (
        <ContextButton
          onClick={() => {
            setShowExitModal(true);
          }}
          Icon={X}
          ariaLabel="Close entry edit"
        />
      )}
      {text.length > 0 ? (
        <Tag className="text-2xl mx-2 font-bold line-clamp-1 text-base-800">
          {text}
        </Tag>
      ) : (
        <Tag className="text-2xl mx-2 font-bold line-clamp-1 text-base-400">
          Empty Title
        </Tag>
      )}
      {viewing === "list" && (
        <div className="flex ml-auto">
          {setSortOption && (
            <ContextButton
              Icon={sortOption === "timestamp-desc" ? SortDesc : SortAsc}
              ariaLabel="Sort entries"
              onClick={() => {
                if (sortOption === "timestamp-desc") {
                  setSortOption("timestamp-asc");
                } else {
                  setSortOption("timestamp-desc");
                }
              }}
            />
          )}
        </div>
      )}
      {viewing === "detail" && (
        <div className="flex ml-auto">
          <ContextButton
            href={`/entry/${entryId}/edit`}
            Icon={Pencil}
            ariaLabel="Edit entry"
          />
          {setShowDeleteModal && (
            <ContextButton
              onClick={() => {
                setShowDeleteModal(true);
              }}
              Icon={Trash2}
              ariaLabel="Delete entry"
            />
          )}
        </div>
      )}
      {viewing === "edit" && (
        <div className="flex ml-auto">
          <ContextButton type="submit" Icon={Check} ariaLabel="Confirm edit" />
        </div>
      )}
    </div>
  );
}
