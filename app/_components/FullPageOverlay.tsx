import { motion, useAnimate, useReducedMotion } from "framer-motion";
import { ChevronLeft, X } from "lucide-react";
import { useRouter } from "next/navigation";
import { useEffect } from "react";

interface FullPageOverlayProps {
  type: "back" | "close";
  buttonLabel?: string;
  onCloseClick?: () => void;
  component: React.ReactNode;
}

export function FullPageOverlay({
  type,
  buttonLabel,
  onCloseClick,
  component,
}: FullPageOverlayProps) {
  const router = useRouter();
  const [card, animateCard] = useAnimate();
  const [overlay, animateOverlay] = useAnimate();

  const reduceMotion = useReducedMotion();

  const onBackClick = () => {
    animateCard(card.current, {
      y: reduceMotion ? 0 : 600,
      opacity: 0,
      duration: 0.5,
    });
    animateOverlay(overlay.current, { opacity: 0, duration: 0.35 });
    setTimeout(() => {
      router.back();
    }, 500);
  };

  useEffect(() => {
    document.body.style.overflowY = "hidden";

    return () => {
      document.body.style.overflowY = "auto";
    };
  }, []);

  return (
    <div className="fixed inset-0 z-50">
      <motion.div
        ref={overlay}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.35, bounce: 0.3, type: "spring" }}
        className="bg-base-200 backdrop-blur-sm bg-opacity-70 dark:bg-opacity-70 w-full h-full"
      />
      <motion.div
        ref={card}
        initial={{ y: reduceMotion ? 0 : -600, opacity: 0 }}
        animate={{ y: 0, opacity: 1 }}
        exit={{ y: reduceMotion ? 0 : 600, opacity: 0 }}
        transition={{ duration: 0.5, bounce: 0.3, type: "spring" }}
        className="fixed inset-0 justify-center items-center bg-base-100 rounded-xl shadow-xl sm:w-[36rem] w-11/12 border border-base-300 flex flex-col gap-3 h-fit max-h-overlaySheet m-auto overflow-hidden"
      >
        <div className="overflow-y-auto p-7 pb-[5.5rem] w-full relative">
          {component}
        </div>
        <div className="flex items-center justify-center absolute bottom-0 w-full z-10 bg-base-100 bg-opacity-50 dark:bg-opacity-50 backdrop-blur border-t border-base-300">
          <button
            className="flex gap-1 items-center text-sm text-base-500 font-semibold p-2 mt-3 mb-4 hover:bg-base-400 hover:bg-opacity-20 hover:dark:bg-opacity-20 active:bg-opacity-30 active:dark:bg-opacity-30 w-fit rounded-md mx-auto active:scale-95 transition-all"
            onClick={type === "back" ? onBackClick : onCloseClick}
          >
            {type === "back" && <ChevronLeft width={20} height={20} />}
            {type === "close" && <X width={20} height={20} />}
            <span className="pr-0.5">
              {type === "back" && (buttonLabel ?? "Go back")}
              {type === "close" && (buttonLabel ?? "Close")}
            </span>
          </button>
        </div>
      </motion.div>
    </div>
  );
}
