"use client";

import { Loader2, Moon, Sun, SunMoon } from "lucide-react";
import { useTheme } from "next-themes";
import { useEffect, useState } from "react";

export function ThemeChanger() {
  const { theme, setTheme } = useTheme();
  const [mounted, setMounted] = useState(false);

  const onClick = () => {
    switch (theme) {
      case "system":
        setTheme("light");
        break;
      case "light":
        setTheme("dark");
        break;
      case "dark":
        setTheme("system");
        break;
      default:
        setTheme("system");
        break;
    }
  };

  useEffect(() => setMounted(true), []);

  if (!mounted)
    return (
      <button
        disabled
        className="flex-none aspect-square p-3 rounded-xl"
        aria-label="Change theme"
      >
        <Loader2 className="animate-spin opacity-50" />
      </button>
    );

  return (
    <button
      aria-label={`Change theme to ${
        theme === "system" ? "light" : theme === "light" ? "dark" : "system"
      }`}
      onClick={onClick}
      className="flex-none aspect-square p-3 hover:bg-base-200 transition-all rounded-xl active:scale-95 focus-visible:bg-base-200 focus-visible:ring-2 focus:border-transparent focus-visible:ring-accent-700 focus-visible:outline-none"
    >
      {theme === "system" && <SunMoon />}
      {theme === "light" && <Sun />}
      {theme === "dark" && <Moon />}
    </button>
  );
}
