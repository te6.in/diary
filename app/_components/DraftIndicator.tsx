import { j } from "@/app/_utils/utils";
import { Archive } from "lucide-react";

interface DraftIndicatorProps {
  absolute?: boolean;
}

export function DraftIndicator({ absolute = false }: DraftIndicatorProps) {
  return (
    <div
      className={j(
        "flex items-center gap-1 bg-amber-100 dark:bg-amber-950 text-amber-500 rounded-md px-2 py-1.5 font-semibold text-sm",
        absolute ? "absolute right-2 top-2" : "ml-auto"
      )}
    >
      <Archive size={20} strokeWidth={2.5} />
      <span>Draft</span>
    </div>
  );
}
