import { EntryDetail } from "@/app/_components/EntryDetail";

interface EntryDetailParmas {
  params: {
    id: string;
  };
}

export default function EntryPage({ params }: EntryDetailParmas) {
  return <EntryDetail entryId={params.id} />;
}
