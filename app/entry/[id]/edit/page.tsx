import { EntryEdit } from "@/app/_components/EntryEdit";

interface EntryEditParmas {
  params: {
    id: string;
  };
}

export default function EntryPage({ params }: EntryEditParmas) {
  return <EntryEdit entryId={params.id} />;
}
