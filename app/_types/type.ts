export const WEATHERS = [
  "sunny",
  "luminous",
  "rainbow",
  "cloudy",
  "drizzling",
  "rainy",
  "thunderstorm",
  "snowy",
] as const;

type Weather = (typeof WEATHERS)[number];

export interface Entry {
  id: string;
  isDraft: boolean;
  title: string;
  body: string;
  timestamp: string;
  createdAt: string;
  weather: Weather;
  tags?: string[];
  media?: {
    images?: string[];
    location?: {
      name: string;
      lat: number;
      lng: number;
    };
    youtube?: {
      url: string;
      title: string;
    };
    appleMusic?: {
      url: string;
      title: string;
    };
  };
}
