import { SortOptions } from "@/app/_components/EntryList";
import { Entry } from "@/app/_types/type";
import { atom } from "recoil";
import { recoilPersist } from "recoil-persist";

const { persistAtom } = recoilPersist();

export const entriesState = atom<Entry[]>({
  key: "entriesState",
  default: [],
  effects_UNSTABLE: [persistAtom],
});

export const sortState = atom<SortOptions>({
  key: "sortState",
  default: "timestamp-desc",
  effects_UNSTABLE: [persistAtom],
});
