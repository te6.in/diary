import { Entry } from "@/app/_types/type";

export function getReadableTimestamp(timestamp: Entry["timestamp"]) {
  const formatter = new Intl.DateTimeFormat("en", {
    hour: "numeric",
    minute: "numeric",
    month: "short",
    day: "numeric",
    year: "numeric",
  });

  const date = new Date(timestamp);

  return formatter.format(date);
}
