import { Entry } from "@/app/_types/type";
import {
  CloudDrizzle,
  CloudLightning,
  CloudRain,
  Cloudy,
  MoonStar,
  Rainbow,
  Snowflake,
  Sun,
} from "lucide-react";

export function getWeatherIcon(name: Entry["weather"]) {
  switch (name) {
    case "sunny":
      return Sun;
    case "cloudy":
      return Cloudy;
    case "rainy":
      return CloudRain;
    case "drizzling":
      return CloudDrizzle;
    case "snowy":
      return Snowflake;
    case "thunderstorm":
      return CloudLightning;
    case "rainbow":
      return Rainbow;
    case "luminous":
      return MoonStar;
    default:
      return Sun;
  }
}

export function getYouTubeVideoId(url: string) {
  const youtubeVideoRegex =
    /(youtu.*be.*)\/(watch\?v=|embed\/|v|shorts|)(.*?((?=[&#?])|$))/gm;

  const match = youtubeVideoRegex.exec(url);

  if (match === null) return null;
  if (match.length < 4) return null;

  return match[3];
}
