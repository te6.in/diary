# Diary (2023-2 Human Computer Interaction Assignment)

![Two screenshots of the live website. Each demonstrates light and dark mode.](https://github.com/te6-in/diary/assets/56245920/bfda4d23-5ed9-4f93-968f-c1c906fbab24)

## Try it out

This website is live at [diary.te6.in](https://diary.te6.in).

## Getting Started

```shell
git clone https://github.com/te6-in/diary.git
cd diary
npm i && npm run dev
```
