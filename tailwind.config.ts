import type { Config } from "tailwindcss";

const defaultTheme = require("tailwindcss/defaultTheme");

const config: Config = {
  darkMode: "class",
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    fontFamily: {
      sans: [
        "Pretendard\\ Variable",
        "Pretendard",
        ...defaultTheme.fontFamily.sans,
      ],
    },
    extend: {
      gridTemplateAreas: {
        layout: ["header", "main"],
        "layout-md": ["header header", "nav main"],
      },
      gridTemplateRows: {
        layout: "auto 1fr",
        "layout-md": "auto 1fr",
      },
      gridTemplateColumns: {
        layout: "1fr",
        "layout-md": "auto 1fr",
      },
      keyframes: {
        shake: {
          "10%, 90%": {
            transform: "translate3d(-1px, 0, 0)",
          },
          "20%, 80%": {
            transform: "translate3d(2px, 0, 0)",
          },
          "30%, 50%, 70%": {
            transform: "translate3d(-3px, 0, 0)",
          },
          "40%, 60%": {
            transform: "translate3d(3px, 0, 0)",
          },
        },
      },
      animation: {
        shake: "shake 0.5s both",
      },
    },
  },
  // FIXME: fonts
  plugins: [
    require("@savvywombat/tailwindcss-grid-areas"),
    require("@tailwindcss/forms"),
  ],
};
export default config;
